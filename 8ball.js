const answers = ["com certeza", "creio que não", "talvez sim", "talvez não"]
let correctAnswer = ""
const answer = document.querySelector("#answer")
const buttonRoll = document.querySelector("#roll")
const instructions = document.querySelector(".instrucoes")
const buttonInstructions = document.querySelector("#btn3")
const buttonOk = document.querySelector("#btn4")

const randomNumber = () => {
    return Math.round(Math.random() * 3)
}

const ballRoll = () => {
    correctAnswer = answers[randomNumber()]
    answer.innerHTML = correctAnswer
}

const showInstructions = () => {
    instructions.style.display = "initial"
}

const undoShowInstructions = () => {
    instructions.style.display = "none"
}

buttonRoll.addEventListener("click", ballRoll)
buttonInstructions.addEventListener("click", showInstructions)
buttonOk.addEventListener("click", undoShowInstructions)
